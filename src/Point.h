// Copyright 2022 Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/****************************************************************************
 * @file Point.h
 * @author Erwan Sturzenegger <erwan.sturzene@edu.hefr.ch>
 *
 * @brief This file contains the declaration of the Point class.
 *
 * @date 05.04.22
 * @version 0.1.0
 ***************************************************************************/

#ifndef CPPALGO_POINT_H
#define CPPALGO_POINT_H

#include <functional>
#include <unordered_set>
#include <ostream>
#include <random>

/**
 * Simple 2D point using doubles. The point can have an optional name.
 * The point implements basic operators to manipulate the point, as well as
 */
class Point {
public:
    /**
     * Constructor
     */
    Point() {};

    /**
     * Destructor
     */
    virtual ~Point() {};

    /**
     * Copy constructor
     * @param point the point to copy
     */
    Point(const Point &point);

    /**
     * Constructor with name and coordinates
     * @param x   the x coordinate
     * @param y   the y coordinate
     * @param name   the name of the point
     */
    Point(double x, double y, std::string name = "");

    /**
     * Getter for the x coordinate
     * @return  the x coordinate
     */
    virtual double getX() const;

    /**
     * Getter for the y coordinate
     * @return  the y coordinate
     */
    virtual double getY() const;

    /**
     * Getter for the name of the point
     * @return  the name of the point
     */
    std::string getName();

    /**
     * Setter for the x coordinate
     * @param x   the x coordinate
     */
    virtual void setX(double x);

    /**
     * Setter for the y coordinate
     * @param y   the y coordinate
     */
    virtual void setY(double y);

    /**
     * Setter for the name of the point
     * @param name   the name of the point
     */
    virtual void setName(std::string name);

    /**
     * Method to calculate the distance between two points
     * @param point   the point to calculate the distance to
     * @return  the distance between the two points
     */
    virtual double distance(const Point &point) const;

    /**
     * Overload of the operator +=
     * @param other   the other point to add
     * @return  the current point
     */
    virtual Point &operator+=(const Point &other);

    /**
     * Overload of the operator -=
     * @param other   the other point to subtract
     * @return  the current point
     */
    virtual Point &operator-=(const Point &other);

    /**
     * Overload of the operator *=
     * @param factor   the factor to multiply by
     * @return  the current point
     */
    virtual Point &operator*=(double other);

    /**
     * Overload of the operator /=
     * @param factor   the factor to divide by
     * @return  the current point
     */
    virtual Point &operator/=(double other);

    /**
     * Overload of the operator +
     * @param other   the other point to add
     * @return  the sum of the two points
     */
    virtual Point operator+(const Point &other);

    /**
     * Overload of the operator -
     * @param other   the other point to subtract
     * @return  the difference of the two points
     */
    virtual Point operator-(const Point &other);

    /**
     * Overload of the operator *
     * @param factor   the factor to multiply by
     * @return  the product of the point and the factor
     */
    virtual Point operator*(double other);

    /**
     * Overload of the operator /
     * @param factor   the factor to divide by
     * @return  the quotient of the point and the factor
     */
    virtual Point operator/(double other);

protected:
    /**
     * The x and y coordinates of the point
     */
    double x, y;
    /**
     * The name of the point
     */
    std::string name;
};

//Needs to be inline if defined in header
std::ostream &operator<<(std::ostream &os, const Point &point);

/**
 * Hashing class that allows to create a hash of a Point easily
 */
class PointHash {
public:
    std::size_t operator()(const Point *point) const {
        std::size_t h1 = std::hash<double>{}(point->getX());
        std::size_t h2 = std::hash<double>{}(point->getY());
        return h1 ^ (h2 << 1); // or use boost::hash_combine
    }
};

/**
 * Create a certain amount of random Points in a specified interval
 * @param count number of points to create
 * @param min minimum x and y value
 * @param max maximum x and y value
 * @param seed seed to be used to rng, -1 to not seed the rng
 * @return
 */
inline std::vector<Point *> createRandomPoints(size_t count, double min, double max, int seed = -1) {
    std::mt19937 random_engine;

    if (seed == -1) {
        std::random_device random_device;
        random_engine = std::mt19937(random_device());
    } else {
        random_engine = std::mt19937(seed);
    }
    std::uniform_int_distribution<int> distribution(min, max);

    std::unordered_set<Point *, PointHash> point_set;

    while (point_set.size() < count) {
        point_set.insert(new Point(distribution(random_engine), distribution(random_engine)));
    }

    return {point_set.begin(), point_set.end()};
}


#endif //CPPALGO_POINT_H
