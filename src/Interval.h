// Copyright 2021 Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/****************************************************************************
 * @file Interval.h
 * @author Erwan Sturzenegger <erwan.sturzene@edu.hefr.ch>
 *
 * @brief Interval class definition and implementation. The interval is a
 * start position, an end position and a value.
 *
 * @date 12.05.22
 * @version 0.1.0
 ***************************************************************************/

#ifndef CPPALGO_INTERVAL_H
#define CPPALGO_INTERVAL_H

/**
 * @brief Class representing an interval
 * @tparam DelimitersType  Type of the delimiters
 * @tparam ValueType     Type of the values
 */
template<typename DelimitersType=int, typename ValueType=double>
class Interval {
public:
    /**
     * @brief Constructor
     * @param start   Start of the interval
     * @param end     End of the interval
     * @param value   Value of the interval
     */
    Interval(DelimitersType start, DelimitersType end, ValueType value = 0) :
            start_(start), end_(end), value_(value) {}

    /**
     * @brief Getter for the start of the interval
     * @return
     */
    DelimitersType getStart() const {
        return start_;
    };

    /**
     * @brief Getter for the end of the interval
     * @return  End of the interval
     */
    DelimitersType getEnd() const {
        return end_;
    };

    /**
     * @brief Getter for the value of the interval
     * @return  Value of the interval
     */
    ValueType getValue() const {
        return value_;
    };

    /**
     * @brief Check if the given position is in the interval
     * @param val   Position to check
     * @return   True if the position is in the interval, false otherwise
     */
    bool contains(DelimitersType val) const {
        return start_ <= val && val <= end_;
    }

    /**
     * @brief Overload of the operator == to compare two intervals.
     * Specialized for the case of the DelimitersType is float and the ValueType is float
     * @param other   Other interval to compare
     * @return True if the intervals are equal, false otherwise
     */
    bool operator==(const Interval<float, float> &other) const {
        return abs(start_ - other.start_) < std::numeric_limits<float>::epsilon() &&
               abs(end_ - other.end_) < std::numeric_limits<float>::epsilon() &&
               abs(value_ - other.value_) < std::numeric_limits<float>::epsilon();
    }

    /**
     * @brief Overload of the operator == to compare two intervals.
     * Specialized for the case of the DelimitersType is double and the ValueType is T
     * @tparam T  Type of the value
     * @param other  Other interval to compare
     * @return  True if the intervals are equal, false otherwise
     */
    template<typename T>
    bool operator==(const Interval<float, T> &other) const {
        return abs(start_ - other.start_) < std::numeric_limits<float>::epsilon() &&
               abs(end_ - other.end_) < std::numeric_limits<float>::epsilon() &&
               value_ == other.value_;
    }

    /**
     * @brief Overload of the operator != to compare two intervals.
     * Specialized for the case of the DelimitersType is T and the ValueType is float
     * @tparam T    Type of the delimiters
     * @param other  Other interval to compare
     * @return  True if the intervals are not equal, false otherwise
     */
    template<typename T>
    bool operator==(const Interval<T, float> &other) const {
        return start_ == other.start_ && end_ == other.end_ &&
               abs(value_ - other.value_) < std::numeric_limits<float>::epsilon();
    }

    /**
     * @brief Overload of the operator != to compare two intervals.
     * Specialized for the case of the DelimitersType is double and the ValueType is double
     * @param other  Other interval to compare
     * @return  True if the intervals are not equal, false otherwise
     */
    bool operator==(const Interval<double, double> &other) const {
        return abs(start_ - other.getStart()) < std::numeric_limits<double>::epsilon() &&
               abs(end_ - other.getEnd()) < std::numeric_limits<double>::epsilon() &&
               abs(value_ - other.getValue()) < std::numeric_limits<double>::epsilon();
    }

    /**
     * @brief Overload of the operator != to compare two intervals.
     * Specialized for the case of the DelimitersType is double and the ValueType is T
     * @tparam T  Type of the value
     * @param other  Other interval to compare
     * @return  True if the intervals are not equal, false otherwise
     */
    template<typename T>
    bool operator==(const Interval<double, T> &other) const {
        return abs(start_ - other.getStart()) < std::numeric_limits<double>::epsilon() &&
               abs(end_ - other.getEnd()) < std::numeric_limits<double>::epsilon() &&
               value_ == other.getValue();
    }

    /**
     * @brief Overload of the operator != to compare two intervals.
     * Specialized for the case of the DelimitersType is T and the ValueType is double
     * @tparam T   Type of the delimiters
     * @param other  Other interval to compare
     * @return  True if the intervals are not equal, false otherwise
     */
    template<typename T>
    bool operator==(const Interval<T, double> &other) const {
        return abs(value_ - other.getValue()) < std::numeric_limits<double>::epsilon() &&
               start_ == other.getStart() && end_ == other.getEnd();
    }

    /**
     * @brief Overload of the operator != to compare two intervals.
     * Generalized version when both the delimiters and the value are not of the type double or float
     * @tparam T1   Type of the delimiters
     * @tparam T2  Type of the value
     * @param other  Other interval to compare
     * @return  True if the intervals are not equal, false otherwise
     */
    template<typename T1, typename T2>
    bool operator==(const Interval<T1, T2> &other) const {
        return start_ == other.getStart() && end_ == other.getEnd() && value_ == other.getValue();
    }

    /**
     * @brief Overload of the operator != to compare two intervals.
     * Use operator== for the comparison and invert it
     * @param other  Other interval to compare
     * @return  True if the intervals are not equal, false otherwise
     */
    bool operator!=(const Interval &other) const {
        return !(*this == other);
    }

private:
    DelimitersType start_; // Start of the interval
    DelimitersType end_;  // End of the interval
    ValueType value_; // Value of the interval

};


#endif //CPPALGO_INTERVAL_H
