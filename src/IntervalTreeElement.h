//
// Created by beatw on 4/29/2022.
//

#ifndef CPPALGO_INTERVALTREEELEMENT_H
#define CPPALGO_INTERVALTREEELEMENT_H

#include <vector>
#include <algorithm>
#include "Interval.h"

/**
 * Single element of the interval tree
 * @tparam T
 */
template<typename T>
class IntervalTreeElement {
public:
    /**
     * Building IntervalTreeElement
     * @param values
     * @param mid
     */
    IntervalTreeElement(std::vector<Interval<T>> &values, T mid) : mid(mid) {
        leftSorted = values;
        rightSorted = values;

        std::sort(leftSorted.begin(), leftSorted.end(), [](const Interval<T> &a, const Interval<T> &b) {
            return a.getStart() < b.getStart();
        });
        std::sort(rightSorted.begin(), rightSorted.end(), [](const Interval<T> &a, const Interval<T> &b) {
            return a.getEnd() > b.getEnd();
        });
    }

    /**
     * Return all Intervals that intersect with a given point
     * @param x
     * @return
     */
    std::vector<Interval<T>> intersecting(T x) {
        std::vector<Interval<T>> result;


        if (x <= mid) {
            for (auto &value: leftSorted) {
                if (value.contains(x)) {
                    result.push_back(value);
                }
            }
        } else {
            for (auto &value: rightSorted) {
                if (value.contains(x)) {
                    result.push_back(value);
                }
            }
        }
        return result;
    }

    std::vector<Interval<T>> leftSorted;
    std::vector<Interval<T>> rightSorted;
    T mid;
};

#endif //CPPALGO_INTERVALTREEELEMENT_H
